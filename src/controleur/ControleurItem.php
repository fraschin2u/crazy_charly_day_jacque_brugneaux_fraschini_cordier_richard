<?php
namespace mywishlist\controleur;

use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\vue\VueItem;

class ControleurItem{
	
	function afficherItems()
    {
        $items = Item::select()->get();
        foreach ($items as $item) {
            echo $item . "<br>";
        }
    }

    function supprimerItem($id){
        $item = Item::select()->where('id', '=', $id)->get()->first();
        $item->delete();
    }

    function afficherItem($num)
    {
        $item = Item::select()->where('id', '=', $num)->get()->first();
        if(isset($item)){
        	$vue = new VueItem(VueItem::$AFFICHER_1_ITEM, $item);
        }
        echo $vue->render();
    }

    function itemVerif($num, $tokenListe){
        $item = Item::select()->where('id', '=', $num)->get()->first();
        $liste = Liste::select()->where('token', '=', $tokenListe)->first();
        if(isset($item) && ($item->liste_id == $liste->no)){
            return true;
        }else return false;
    }
	
	function reserverItem($id)
	{
        $item=Item::select()->where('id','=',$id)->first();
        $item->reserve="reservé";
        $item->email=$_SESSION['email'];
        $item->save();
	}

	function dereserverItem($id)
	{
	    $item=Item::select()->where('id','=',$id)->first();
	    $item->reserve="non reservé";
	    unset($item->email);
	    $item->save();
	}
	
	function createurItem($id_liste){
		$vue=new VueItem(VueItem::$CREATION_ITEM, $id_liste);
        echo $vue->render();
	}
	
	function annulerReservation($idList, $idItem)
	{
		$item = Item::select()->where('id', '=', $idItem)->first();
		if($item->liste_id == $idList){
			$item->liste_id = 0;
		}
	}

	function ajouterItem($liste_id, $nom, $descr, $url, $tarif){
        if($tarif<1000){
            $app = \Slim\Slim::getInstance();
            $dossier = '/www/jacque14u/ProjetPhp/web/img';
            $fichier = basename($_FILES['mon_image']['name']);
            $taille_maxi = 100000;
            $taille = filesize($_FILES['mon_image']['tmp_name']);
            $extensions = array('.png', '.gif', '.jpg', '.jpeg');
            $extension = strrchr($_FILES['mon_image']['name'], '.');
            //D�but des v�rifications de s�curit�...
            if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
            {
                $erreur = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg, txt ou doc...';
            }
            if($taille>$taille_maxi)
            {
                $erreur = 'Le fichier est trop gros...';
            }
            if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
            {
                //On formate le nom du fichier ici...
                $fichier = strtr($fichier,
                    '����������������������������������������������������',
                    'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
                $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
                if(move_uploaded_file($_FILES['mon_image']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que �a a fonctionn�...
                {
                    
                }
                
            }
            
            $i = new Item();
            $i->liste_id = $liste_id;
            $i->nom = $nom;
            $i->descr = $descr;
            $i->url = $url;
            $i->tarif = $tarif;
            $i->img = "$fichier";
            $i->save();
        }else {
            $_SESSION['erreur']['tarifItem'] = true;
        }
	}
}
