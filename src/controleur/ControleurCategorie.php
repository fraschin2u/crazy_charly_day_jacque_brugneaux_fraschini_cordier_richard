<?php
namespace mywishlist\controleur;

use mywishlist\models\Categorie;
use mywishlist\models\User;
use mywishlist\vue\VueCategorie;
class ControleurCategorie
{

    
    function afficherCategories(){
        $modele=Categorie::select()->get();
        $vue=new VueCategorie(VueCategorie::$AFFICHER_CATEGORIES, $modele);
        echo $vue->render();
    }
    
    
}